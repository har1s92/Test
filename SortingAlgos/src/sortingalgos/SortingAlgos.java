/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sortingalgos;

import java.util.Arrays;
import java.util.Random;

/**
 * @author Haris PID 3747343
 */
public class SortingAlgos {

    public static int length = 20000; // set length to 20000
    public static int[] myOriginalUnsortedArray1 = new int[length]; 
    public static int[] myCopyUnsortedArray2 = new int[length];
    public static long bubbleSortDuration, quickSortDuration, selectionSortDuration, insertionSortDuration, mergeSortDuration;

    /**
     * @param args the command line arguments
     *Calling all the methods in main 
     */
    public static void main(String[] args) {

        generateRanNums();
        copyRanNums();
        bubbleSort();
        copyRanNums();
        selectionSort();
        copyRanNums();
        insertionSort();
        compareTime();
    }

    /**
     * Copy original array to copyUnsorted array
     */
    private static void copyRanNums() {
        myCopyUnsortedArray2 = Arrays.copyOf(myOriginalUnsortedArray1, length);
    }

    /**
     * Iterate loop till the length of the array Generate a random number
     * between 1 and 5000 5001 is upper bound, 1 is lower bound
     */
    public static void generateRanNums() {
        Random rand = new Random(); // Create object of random class
        int x;
        for (int i = 0; i < length; i++) {
            x = rand.nextInt(5001 - 1) + 1;
            myOriginalUnsortedArray1[i] = x; // Store that value in array
        }
    }

    /**
     * Compare each element with every other element in array, 
     * if less, than swap
     */
    private static void bubbleSort() {

        long startTime = System.nanoTime(); //time starts in milli seconds
        int temp;
        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < length; j++) {

                if (myCopyUnsortedArray2[i] > myCopyUnsortedArray2[j]) {
                    temp = myCopyUnsortedArray2[i];
                    myCopyUnsortedArray2[i] = myCopyUnsortedArray2[j];
                    myCopyUnsortedArray2[j] = temp;
                }
            }
        }
        long endTime = System.nanoTime(); //time ends

        bubbleSortDuration = endTime - startTime; // subtract the end time with start time and store it in bubbleSortDuration
    }

    /**
     * At the end of nested loop, minIndex contains the index of minimum
     * element, swap it with index i and now first element is sorted.
     * Same goes for next iterations.
     *
     */
    private static void selectionSort() {
        long startTime = System.nanoTime();
        int temp;
        for (int i = 0; i < length - 1; i++) {
            int minIndex = i; // Variable to hold index of element with minimum value
            for (int j = i + 1; j < length; j++) {
                if (myCopyUnsortedArray2[j] < myCopyUnsortedArray2[minIndex]) {
                    minIndex = j;
                }
            }

            temp = myCopyUnsortedArray2[minIndex];
            myCopyUnsortedArray2[minIndex] = myCopyUnsortedArray2[i];
            myCopyUnsortedArray2[i] = temp;
        }
        long endTime = System.nanoTime();
        selectionSortDuration = endTime - startTime;
    }

    /**
     * Insertion sort method starts the loop with second element. temp contains
     * element to be compared with j starts with first element compare current
     * element with temp and swap with adjacent element
     */
    private static void insertionSort() {
        long startTime = System.nanoTime();
        int temp;
        for (int i = 1; i < length; i++) {
            temp = myCopyUnsortedArray2[i];
            int j = i - 1;

            while (j >= 0 && myCopyUnsortedArray2[j] > temp) {
                myCopyUnsortedArray2[j + 1] = myCopyUnsortedArray2[j];
                j -= 1;
            }
            myCopyUnsortedArray2[j + 1] = temp;
        }

        long endTime = System.nanoTime();
        insertionSortDuration = endTime - startTime;

    }

    /**
     * Print execution time for all the sorts in ascending order Check which is
     * fastest duration between bubbleSort, selectionSort, and insertionSort
     * print the one with that has the fastest time duration
     */
    private static void compareTime() {

        System.out.println("Bubble Sort time : " + bubbleSortDuration + " ns");

        System.out.println("Selection Sort time : " + selectionSortDuration + " ns");

        System.out.println("Insertion Sort time : " + insertionSortDuration + " ns");

        if (bubbleSortDuration < selectionSortDuration && bubbleSortDuration < insertionSortDuration) {
            System.out.println("***Bubble sort is the fastest algorithm***");
        } else if (selectionSortDuration < bubbleSortDuration && selectionSortDuration < insertionSortDuration) {
            System.out.println("***Selection sort is the fastest algorithm***");
        } else {
            System.out.println("***Insertion sort is the fastest algorithm***");
        }
    }

    public static void mergeSort() {
//extra credit
    }

    public static void quickSort() {
//extra credit
    }
}
